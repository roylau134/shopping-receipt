package cli;

import java.util.Scanner;

import controller.ResultController;
import emun.LocationType;
import emun.SalesType;
import helper.CostHelper;
import model.Result;
import model.SalesItem;

public class CLI {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Weclome to Shopping Receipt");

		// Handle location info
		LocationType locationType;
		String locationTypeString;
		while (true) {
			System.out.print("Please select your US states (1 for California, 2 for New York): ");
			try {
				Scanner sc = new Scanner(System.in);
				int intInput = sc.nextInt();
				switch (intInput) {
				case 1:
					locationType = LocationType.CA;
					locationTypeString = "California";
					break;
				case 2:
					locationType = LocationType.NY;
					locationTypeString = "New York";
					break;
				default:
					throw new Exception("Int is not correct");
				}
				break;
			} catch (Exception e) {
				System.out.println("*** Error: US states selecting number is not correct, please try again ***");
				System.out.println();
				continue;
			}
		}
		System.out.println(String.format("Selected as \"%s\"", locationTypeString));

		// start record data
		ResultController resultController = new ResultController(locationType);
		for (int counter = 1; true; counter++) {
			System.out.println(String.format("------- Item #%d -------", counter));

			// handle name
			System.out.print("Please type the iten name: ");
			Scanner sc = new Scanner(System.in);
			String nameInput = sc.nextLine().trim();

			// handle price
			double priceInput;
			while (true) {
				System.out.print("Please type the iten unit price: $");
				try {
					sc = new Scanner(System.in);
					priceInput = sc.nextDouble();
					if (priceInput <= 0.0) {
						throw new Exception("Input less than or equal to 0");
					}
					priceInput = CostHelper.To2Dec(priceInput);
					break;

				} catch (Exception e) {
					System.out.println("*** Error: Unit price is not correct, please try again ***");
					System.out.println();
					continue;
				}
			}

			// handle qty
			int qtyInput;
			while (true) {
				System.out.print("Please type the iten qty: ");
				try {
					sc = new Scanner(System.in);
					qtyInput = sc.nextInt();
					if (qtyInput <= 0) {
						throw new Exception("Input less than or equal to 0");
					}
					break;

				} catch (Exception e) {
					System.out.println("*** Error: Qty is not correct, please try again ***");
					System.out.println();
					continue;
				}
			}

			// confirm message
			boolean isAdded = false;
			while (true) {
				String priceString = String.format("$%.2f", CostHelper.To2Dec(priceInput));

				System.out.println("Please confirm the below item is correct.");
				System.out.println(String.format("%-7s %20s", "Name:", nameInput));
				System.out.println(String.format("%-7s %20s", "Price:", priceString));
				System.out.println(String.format("%-7s %20d", "Qrt:", qtyInput));
				System.out.println("Correct? (y for Yes, n for No)");
				try {
					sc = new Scanner(System.in);
					String stringInput = sc.nextLine().trim().toLowerCase();
					switch (stringInput) {
					case "y":
						resultController.Add(nameInput, priceInput, qtyInput);
						isAdded = true;
						break;
					case "n":
						break;
					default:
						throw new Exception("Type is not correct");
					}
					break;
				} catch (Exception e) {
					System.out.println("*** Error: Country number is not correct, please try again ***");
					System.out.println();
					continue;
				}
			}

			// handle stop or process
			boolean isContinue = false;
			if (isAdded) {// stop
				while (true) {
					System.out.println("Is continue to add item?");
					try {
						sc = new Scanner(System.in);
						String stringInput = sc.nextLine().trim().toLowerCase();
						switch (stringInput) {
						case "y":
							isContinue = true;
							break;
						case "n":
							break;
						default:
							throw new Exception("Type is not correct");
						}
						break;
					} catch (Exception e) {
						System.out.println("*** Error: Request is not correct, please try again ***");
						System.out.println();
						continue;
					}
				}
			} else {// process
				System.out.println("Since droped the current record, please retry to add item.");
				counter--;
				continue;
			}

			if (isContinue) {
				System.out.println();
				continue;
			} else {
				break;
			}
		}

		// print receipt
		Result result = resultController.GetResult();

		System.out.println();
		System.out.println("~~~~~~~~~~~~~~~~~ Recepit ~~~~~~~~~~~~~~~~");
		PrintReceptItem("Items:", "Price:", "Qty:");
		for (SalesItem item : result.salesItems) {
			PrintReceptItem(item.name, item.cost, item.qty);
		}
		String subTotalString = String.format("$%.2f", result.subTotal);
		String taxString = String.format("$%.2f", result.tax);
		String totalString = String.format("$%.2f", result.total);

		PrintReceptItem("Subtotal:", "", subTotalString);
		PrintReceptItem("Tax:", "", taxString);
		PrintReceptItem("Total:", "", totalString);

	}

	private static void PrintReceptItem(String item, double price, int qty) {
		String priceString = String.format("$%.2f", price);
		String qtyString = Integer.toString(qty);
		PrintReceptItem(item, priceString, qtyString);
	}

	private static void PrintReceptItem(String item, String price, String qty) {
		System.out.println(String.format("%-20s %10s %10s", item, price, qty));
	}
}
