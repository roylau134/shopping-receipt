package controller;

import java.util.List;

import emun.LocationType;
import emun.SalesType;
import helper.CostHelper;
import model.Result;
import model.SalesItem;

public class ResultController {
	private final Result result = new Result();
	private final LocationType locationType;

	public ResultController(LocationType locationType) {
		this.locationType = locationType;
	}

	public void Add(SalesItem salesItem) {
		result.salesItems.add(salesItem);
	}

	public void Add(String name, double cost, int qty) {
		SalesType type = CostHelper.GetProductCategory(name);
		SalesItem item = new SalesItem(name, type, cost, qty);
		Add(item);
	}

	public Result GetResult() {
		UpdateResult();
		return result;
	}

	private void UpdateResult() {
		double subTotal = 0;
		double tax = 0;
		double total = 0;

		for (SalesItem item : result.salesItems) {
			double tempTotal = CostHelper.GetSubTotal(item.cost, item.qty);
			subTotal += tempTotal;

			double tempTax = CostHelper.GetTax(item.cost, item.qty, item.type, locationType);
			tax += tempTax;
		}

		tax = CostHelper.GetRoundedTax(tax);
		total = subTotal + tax;

		result.subTotal = subTotal;
		result.tax = tax;
		result.total = total;
	}

}
