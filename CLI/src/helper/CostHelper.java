package helper;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import emun.LocationType;
import emun.SalesType;

public class CostHelper {
	private static DecimalFormat costDecimalFormat = new DecimalFormat("0.00");

	public static double To2Dec(double data) {
		return Double.parseDouble(CostHelper.costDecimalFormat.format(data));
	}

	private static final Map<LocationType, Double> locationTax = new HashMap<LocationType, Double>() {
		{
			put(LocationType.CA, 0.0975);
			put(LocationType.NY, 0.08875);
		}
	};

	private static final Map<LocationType, List<SalesType>> locationSalesTypeToBeExcluded = new HashMap<LocationType, List<SalesType>>() {
		{
			put(LocationType.CA, List.of(SalesType.Food));
			put(LocationType.NY, List.of(SalesType.Food, SalesType.Clothing));
		}
	};

	private static final Map<String, SalesType> productCategories = new HashMap<String, SalesType>() {
		{
			put("book", SalesType.Others);
			put("potato chips", SalesType.Food);
			put("pencil", SalesType.Others);
			put("shirt", SalesType.Clothing);
		}
	};

	public static double GetLocationTaxRate(LocationType locationType) {
		return locationTax.get(locationType);
	}

	public static List<SalesType> GetLocationSalesTypeToBeExcluded(LocationType locationType) {
		return locationSalesTypeToBeExcluded.get(locationType);
	}

	public static SalesType GetProductCategory(String productName) {
		SalesType result = productCategories.get(productName);
		if (result == null) {
			throw new Error("Unable to lookup product cat");
		}
		return result;
	}

	public static double GetRoundedTax(double tax) {
		Double tempTax = tax * 10;
		Double tempTaxDec = tempTax - Math.floor(tempTax);
		double offSet = 0.0;
		double tax2Dec = 0.0;
		if (tempTaxDec > 0.5) {
			offSet = 0.1;
			tax2Dec = 0.0;
		} else if (tempTaxDec > 0.0) {
			offSet = 0.0;
			tax2Dec = 0.05;
		} else {
			offSet = 0.0;
			tax2Dec = 0.0;
		}
		return new Double(Math.floor(tax * 10)) / 10 + offSet + tax2Dec;
	}

	public static double GetTax(double cost, double qty, SalesType salesType, LocationType locationType) {
		List<SalesType> salesTypeToBeExcluded = locationSalesTypeToBeExcluded.get(locationType);
		if (!salesTypeToBeExcluded.contains(salesType)) {
			return cost * qty * locationTax.get(locationType);
		}
		return 0.0;
	}

	public static double GetSubTotal(double cost, double qty) {
		return cost * qty;
	}

}
