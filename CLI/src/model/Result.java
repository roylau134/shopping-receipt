package model;

import java.util.ArrayList;
import java.util.List;

import emun.LocationType;
import emun.SalesType;
import helper.CostHelper;

public class Result {
	public final List<SalesItem> salesItems = new ArrayList<SalesItem>();
	public double subTotal = 0;
	public double tax = 0;
	public double total = 0;
}
