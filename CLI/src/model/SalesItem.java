package model;

import emun.SalesType;

public class SalesItem {
	public final String name;
	public final SalesType type;
	public final double cost;
	public final int qty;

	public SalesItem(String name, SalesType type, double cost, int qty) {
		this.name = name;
		this.type = type;
		this.cost = cost;
		this.qty = qty;
	}

}
